#!/usr/env/bin python

class Persona():
    def __init__(self, nombre):
        self.nombre = nombre
        self.apellido = None
        self._edad = 10
        self.set_apellido()
        self.get_apellido()

    def set_apellido(self):
        self.apellido = "Perez"

    def get_apellido(self):
        return self.apellido

    @property
    def edad(self):
        return self._edad

    @edad.setter
    def edad(self, value):
        if value > 18:
            print("Mayor de edad")
        self._edad = value

pepito = Persona("Pepito")
print(pepito.nombre, " ", pepito.apellido)

juanito = Persona("Juanito")
print(juanito.nombre, " ", juanito.get_apellido())

print(juanito.edad)
juanito.edad = 26
print(juanito.edad)

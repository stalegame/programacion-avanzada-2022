#!/usr/bin/env python3

""" De acuerdo a la siguiente imagen desarrolle una aplicación
orientada a objeto definiendo atributos, métodos.  """


class Protein:
    def __init__(self):
        self.protein_name = None
        self.protein_description = None
        self.pdb_doi = None
        self.protein_classification = None
        self.protein_organism = None
        self.protein_expresion_system = None
        self.protein_mutation = None
        self.protein_deposited = None
        self.protein_released = None
        self.protein_deposited_author = None
        self.funding_organization = None

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# Se crea la clase donde se programara todos los elementos del glade
class ventana_principal():

    def __init__(self):

        # Creador y llamar a esté
        builder = Gtk.Builder()
        builder.add_from_file("interfaz_guia_1.ui")

        # Creacion de la ventana principal
        ventana = builder.get_object("principal")
        ventana.connect("destroy", Gtk.main_quit)
        ventana.set_title("Guia")
        ventana.maximize()

        # Entradas de texto
        self.entrada_1 = builder.get_object("entrada_texto_1")
        self.entrada_1.connect("activate", self.sumar_texto)
        self.entrada_1.set_placeholder_text("Ingrese texto o numero")

        self.entrada_2 = builder.get_object("entrada_texto_2")
        self.entrada_2.connect("activate", self.sumar_texto)
        self.entrada_2.set_placeholder_text("Ingrese texto o numero")

        # label
        label_text = builder.get_object("label_1")
        label_text.set_label("entrada: ")

        label_text_2 = builder.get_object("label_2")
        label_text_2.set_label("entrada: ")

        self.suma = builder.get_object("label_3")
        self.suma.set_label("suma: ")

        # Botones
        self.deshacer = builder.get_object("deshacer_btn")
        self.deshacer.connect("clicked", self.limpiar)

        self.aceptar = builder.get_object("aceptar_btn")
        self.aceptar.connect("clicked", self.dialogo)

        ventana.show_all()

    """ Esta funcion se encarga de verificar si lo que se ingresa es una cadena
        de caracteres o numerica, para identifica su largo o usar el numero
        como tal"""

    def sumar_texto(self, enter=None):

        self.text1 = self.entrada_1.get_text()
        self.text2 = self.entrada_2.get_text()

        if self.text1.isnumeric():
            valor = int(self.text1)
        else:
            valor = len(self.text1)

        if self.text2.isnumeric():
            valor_2 = int(self.text2)
        else:
            valor_2 = len(self.text2)

        suma = valor + valor_2
        self.suma.set_label(f'suma: {str(suma)}')


    """ Esta funcion llama a la funcion creada para operar en la ventana de
        dialogo creada y mediante lo pedido en la guia hacer que los botones
        respondan a ciertas acciones"""

    def dialogo(self, btn=None):

        self.sumar_texto()
        self.ventana_dialogo = dialogo(self.text1, self.text2, self.suma)

        response = self.ventana_dialogo.ventana.run()

        if response == Gtk.ResponseType.OK:

            if self.text1.isnumeric():
                self.entrada_1.set_text("0")
            else:
                self.entrada_1.set_text("")

            if self.text2.isnumeric():
                self.entrada_2.set_text("0")
            else:
                self.entrada_2.set_text("")

        self.ventana_dialogo.ventana.destroy()

    # Se encarga de limpiar las entradas de texto
    def limpiar(self, btn=None):
        self.entrada_1.set_text("")
        self.entrada_2.set_text("")
        self.suma.set_label("suma:")


# Se crea la clase para la ventana de dialogo
class dialogo():


    def __init__(self, text1, text2, suma):

        # contructor
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz_guia_1.ui")

        # ventana dialogo
        self.ventana = self.builder.get_object("ventana_resultados")

        # Se utiliza la la siguiente forma para mostrar texto 
        self.ventana.format_secondary_text("Se ha ingresado los textos "
                                           f"{text1} y {text2} \n"
                                           "Cuyos valores suman: "
                                           f"{suma.get_text()}")
        self.ventana.show_all()


if __name__ == "__main__":
    ventana_principal()
    Gtk.main()
